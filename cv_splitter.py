import pandas as pd
from sklearn.model_selection import train_test_split

if __name__ == '__main__':
    dataset = 'OZg3s125'
    X = pd.read_csv('dataset/noise_clean/OZ_g3_s125.csv', header=None)
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]

    for k in range(10):
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=k)
        pd.DataFrame(X_train).to_csv('BooSt/' + dataset + '_X_train_' + str(k) + '.csv', index=False, header=False)
        pd.DataFrame(y_train).to_csv('BooSt/' + dataset + '_Y_train_' + str(k) + '.csv', index=False, header=False)
        pd.DataFrame(X_test).to_csv('BooSt/' + dataset + '_X_test_' + str(k) + '.csv', index=False, header=False)
        pd.DataFrame(y_test).to_csv('BooSt/' + dataset + '_Y_test_' + str(k) + '.csv', index=False, header=False)
