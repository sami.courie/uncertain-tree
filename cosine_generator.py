from sklearn import tree
import numpy as np
import time
import scipy.stats as stats
import pandas as pd
import sys

if __name__ == '__main__':

    np.random.seed(42)
    X = np.random.uniform(low=0, high=5, size=1100).reshape((1100, 1))
    noise = np.random.normal(loc=0, scale=0.05, size=1100).reshape((1100, 1))
    Y_real = np.cos(X)
    Y = np.cos(X) + noise
    method = 'mseprob'

    X_train = X[:100]
    y_train = Y[:100].flatten()
    y_real = Y_real[100:]
    X_test = X[100:]
    y_test = Y[100:].flatten()

    sigma_Xp = np.std(X_train, axis=0)
    min_samples_leaf = round(len(X_train) * 0.2)

    if method == 'mse':
        regressor = tree.DecisionTreeRegressor(criterion=method, random_state=0,
                                               min_samples_leaf=min_samples_leaf, tol=sigma_Xp)
    else:
        regressor = tree.DecisionTreeRegressor(criterion=method, random_state=0, splitter='topk1',
                                               min_samples_leaf=min_samples_leaf, tol=sigma_Xp)
    t = time.process_time()
    regressor.fit(X_train, y_train)
    # print('Node count', regressor.tree_.node_count)
    t = time.process_time() - t

    if method == 'mse':
        prediction = regressor.predict(X_test)
    else:
        F = [f for f in regressor.tree_.feature if f != -2]
        for s_current_node in range(len(F)):
            for kk in range(s_current_node + 1, len(F)):
                if F[s_current_node] == F[kk]:
                    F[kk] = -1
        F = np.array(F)
        prediction = regressor.predict3(X_test, F=F)

    error = abs(y_test - prediction)
    RMSE_test = np.sqrt(np.mean(error ** 2))
    print(RMSE_test, regressor.tree_.node_count)
    new_file = np.c_[X_test, y_real, prediction]
    # pd.DataFrame(new_file).to_csv('prediction_uniform_mse.csv', index=False, header=False)
    # tree.export_graphviz(regressor, out_file='cosine.dot')

    left_values = [float("-inf"), 1.43, 2.472, 3.868]
    right_values = [1.43, 2.472, 3.868, float("inf")]

    gmma = np.linalg.pinv(regressor.tree_.preg.T.dot(regressor.tree_.preg)).dot(regressor.tree_.preg.T).dot(y_train)
    regressor.tree_.value[1] = gmma[0]
    regressor.tree_.value[3] = gmma[1]
    regressor.tree_.value[5] = gmma[2]
    regressor.tree_.value[6] = gmma[3]
    prob_matrix = []
    sigma_2 = sigma_Xp[0] / 2
    for val in X_test:
        g = stats.norm(loc=val[0], scale=sigma_2)
        temp_arr = [val[0]]
        for region_id in range(4):
            left_f = left_values[region_id]
            right_f = right_values[region_id]
            proba = g.cdf(right_f) - g.cdf(left_f)
            temp_arr.append(proba)
        prob_matrix.append(temp_arr)

    prob_matrix = np.array(prob_matrix)
    # pd.DataFrame(prob_matrix).to_csv('proba_matrix_20_sg2.csv', index=False, header=False)
    # tree.export_graphviz(regressor, out_file='cosine_sg2.dot', impurity=False, feature_names=['X'])
