import sys
import csv
import pandas as pd
from sklearn import tree
import numpy as np
import time
import os
from sklearn.model_selection import train_test_split
import argparse


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-y', type=str, help='y path')
    parser.add_argument('-f', type=str, help='name')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-p', type=int, help='header', default=0)
    parser.add_argument('-i', type=int, help='index column', default=None)
    parser.add_argument('-n', type=int, help='normalizer', default=None)
    parser.add_argument('-t', type=int, help='target column', default=None)
    parser.add_argument('-s', type=str, help='splitting method', default=None)
    parser.add_argument('-m', type=str, help='criterion method', default='mseprob')
    parser.add_argument('-l', type=float, help='min leaf percentage', default=0.1)
    parser.add_argument('-d', type=int, help='max depth', default=None)
    parser.add_argument('-r', type=str, help='delimeter', default=',')
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    parser.add_argument('-ts', type=float, help='test size in percentage', default=0.2)
    args = parser.parse_args()

    args.x = 'dataset/noise_clean/OZ_g3_s125.csv'
    args.f = 'OZg3s125'
    if args.p:
        X = pd.read_csv(args.x, delimiter=args.r, index_col=args.i)
    else:
        X = pd.read_csv(args.x, header=None, delimiter=args.r, index_col=args.i)
    # Convert pandas into nd array

    X = X.values

    folder = 'soft-tree/' + args.f
    os.mkdir(folder)

    if args.y:
        if args.p:
            Y = pd.read_csv(args.y, delimiter=args.r, index_col=args.i)
        else:
            Y = pd.read_csv(args.y, header=None, delimiter=args.r, index_col=args.i)
        Y = Y.values
        Y = Y.ravel()
    else:
        if args.t:
            Y = X[:, 0]
            X = X[:, 1:X.shape[1]]
        else:
            Y = X[:, -1]
            X = X[:, 0:X.shape[1] - 1]

    for k in range(args.cvs, args.cve):
        m = (k / 2) + 1
        n = (k % 2) + 1
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=args.ts, random_state=k)

        X_valid_tr, X_valid_test, y_valid_tr, y_valid_test = train_test_split(X_train, y_train,
                                                                              test_size=args.ts, random_state=0)

        new_train = np.c_[X_valid_tr, y_valid_tr]
        new_valid = np.c_[X_valid_test, y_valid_test]
        new_test = np.c_[X_test, y_test]

        pd.DataFrame(new_train).to_csv(folder + '/' + args.f + '-train-' + str(k) + '.txt',
                                       index=False, header=False, sep=' ')
        pd.DataFrame(new_valid).to_csv(folder + '/' + args.f + '-validation-' + str(k) + '.txt',
                                       index=False, header=False, sep=' ')
        pd.DataFrame(new_test).to_csv(folder + '/' + args.f + '-test-' + str(k) + '.txt',
                                      index=False, header=False, sep=' ')
