import csv
import pandas as pd
from sklearn import tree
import numpy as np
import time
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
import argparse
import random


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-y', type=str, help='y path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-p', type=int, help='header', default=0)
    parser.add_argument('-i', type=int, help='index column', default=None)
    parser.add_argument('-n', type=int, help='normalizer', default=None)
    parser.add_argument('-t', type=int, help='target column', default=None)
    parser.add_argument('-s', type=str, help='splitting method', default=None)
    parser.add_argument('-m', type=str, help='criterion method', default='mseprob')
    parser.add_argument('-l', type=float, help='min leaf percentage', default=0.1)
    parser.add_argument('-d', type=int, help='max depth', default=None)
    parser.add_argument('-r', type=str, help='delimeter', default=',')
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    parser.add_argument('-ts', type=float, help='test size in percentage', default=0.2)
    args = parser.parse_args()

    args.x = 'dataset/To_Predict/transcoding_mesurment.csv'

    if args.p:
        X = pd.read_csv(args.x, delimiter=args.r, index_col=args.i)
    else:
        X = pd.read_csv(args.x, header=None, delimiter=args.r, index_col=args.i)
    # Convert pandas into nd array

    X = X.values

    if args.y:
        if args.p:
            Y = pd.read_csv(args.y, delimiter=args.r, index_col=args.i)
        else:
            Y = pd.read_csv(args.y, header=None, delimiter=args.r, index_col=args.i)
        Y = Y.values
        Y = Y.ravel()
    else:
        if args.t:
            Y = X[:, 0]
            X = X[:, 1:X.shape[1]]
        else:
            Y = X[:, -1]
            X = X[:, 0:X.shape[1] - 1]

    outputs = []
    times = []
    rmses = []
    all_errors = []
    all_predictions = []
    all_y = []

    for ind, k in enumerate(range(args.cvs, args.cve)):

        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=args.ts, random_state=k)

        print(k)
        sigma_Xp = np.std(X_train, axis=0)
        min_samples_leaf = round(len(X_train) * args.l)

        var_size = random.randrange(20, 30) / 100
        var_size = round(var_size * X_train.shape[1])
        var_size = max(var_size, 3)
        regressor = RandomForestRegressor()
        t = time.process_time()
        regressor.fit(X_train, y_train)
        t = time.process_time() - t

        prediction = regressor.predict(X_test)

        all_predictions.extend(prediction)
        all_y.extend(y_test)
        error = abs(y_test - prediction)
        all_errors.extend(error)
        RMSE_test = np.sqrt(np.mean(error ** 2))
        print(RMSE_test)
        outputs.append([k])
        outputs.append([RMSE_test])
        outputs.append([t])
        outputs.append([])

        times.append(t)
        rmses.append(RMSE_test)

        """
        with open(args.o, 'w') as csv_file:
            spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for output in outputs:
                spam_writer.writerow(output)
        """

    all_errors = np.array(all_errors)
    all_predictions = np.array(all_predictions)
    all_y = np.array(all_y)
    outputs.append(['Average MSE', np.mean(rmses)])
    outputs.append(['STD', np.std(rmses)])
    print('Average MSE', np.mean(rmses))
    print('STD', np.std(rmses))
    outputs.append(['Average Time', np.mean(times)])
    outputs.append(['Total Average MSE', np.sqrt(np.mean(all_errors ** 2))])

    """
    with open(args.o, 'w') as csv_file:
        spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for output in outputs:
            spam_writer.writerow(output)
    """
    all_errors = all_errors.T
    all_predictions = all_predictions.T
    all_y = all_y.T
    """
    pd.DataFrame(all_errors).to_csv(args.e, index=False, header=False)
    pd.DataFrame(all_y).to_csv(args.e.replace('error', 'y'), index=False, header=False)
    pd.DataFrame(all_predictions).to_csv(args.e.replace('error', 'prediction'), index=False, header=False)
    """
