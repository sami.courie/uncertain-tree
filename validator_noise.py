import sys
import csv
import pandas as pd
from sklearn import tree
import numpy as np
import time
from sklearn.model_selection import train_test_split
import argparse


def add_noise(arr):

    arr_size = len(arr)
    sigma_x = np.std(X, axis=0)
    mean_x = np.mean(X, axis=0)
    print(mean_x)
    print(sigma_x)
    for ind, sigma in enumerate(sigma_x):
        sigma_low = sigma
        sigma_high = sigma * 1.25
        np.random.seed(0)
        noise_arr = np.random.uniform(low=sigma_low, high=sigma_high, size=arr_size)
        sign_arr = np.random.uniform(low=0, high=1, size=arr_size)
        negative_arr = np.where(sign_arr <= 0.5)[0]
        noise_arr[negative_arr] *= -1
        arr[:, ind] += noise_arr
    return arr


def add_noise_gaussian(arr):

    arr_size = len(arr)
    sigma_x = np.std(X, axis=0)
    mean_x = np.mean(X, axis=0)
    print(mean_x)
    print(sigma_x)
    for ind, sigma in enumerate(sigma_x):
        sigma_high = sigma * 1.25
        np.random.seed(3)
        noise_arr = np.random.normal(loc=mean_x[ind], scale=sigma_high, size=arr_size)
        sign_arr = np.random.uniform(low=0, high=1, size=arr_size)
        negative_arr = np.where(sign_arr <= 0.5)[0]
        noise_arr[negative_arr] *= -1
        arr[:, ind] += noise_arr
    return arr


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    args = parser.parse_args()
    args.x = 'dataset/clean/Ozone.csv'

    X = pd.read_csv(args.x, header=None, index_col=None)
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]

    new_X = add_noise_gaussian(X)
    new_X = np.c_[new_X, Y]
    new_X = new_X.round(decimals=5)
    pd.DataFrame(new_X).to_csv('dataset/noise_clean/OZ_g3_s125.csv', index=False, header=False)
