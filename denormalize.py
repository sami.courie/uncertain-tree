import pandas as pd
import numpy as np


def denormalize_set(name):
    errors = []
    rmses = []
    for i in range(10):
        new_name = 'data/n-' + name + '/n-' + name + '-train-' + str(i) + '.txt'
        y = pd.read_csv(new_name, header=None, delimiter=' ')
        y = y.values
        y = y[:, -1]
        y_mean = np.mean(y)
        y_std = np.std(y, ddof=1)

        x = pd.read_csv('results/noisy/v-n-' + name + '-' + str(i), header=None)
        x = x.values.ravel()
        x = x[:-2]
        x = x * y_std
        x = x + y_mean

        new_name = 'data/n-' + name + '/n-' + name + '-test-' + str(i) + '.txt'
        y_test = pd.read_csv(new_name, header=None, delimiter=' ')
        y_test = y_test.values[:, -1]

        error = np.abs(y_test - x)
        RMSE_test = np.sqrt(np.mean(error ** 2))
        rmses.append(RMSE_test)
        errors.extend(error)

    rmses = np.array(rmses)
    print('Average MSE', np.mean(rmses))
    print('STD', np.std(rmses))
    errors = np.array(errors)
    pd.DataFrame(errors).to_csv('error/noisy/' + name + '_soft_error.csv', index=False, header=False)


if __name__ == '__main__':

    denormalize_set('oz')